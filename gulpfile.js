'use strict';

const gulp = require('gulp');
const sass = require('gulp-sass');
const cssnano = require('gulp-cssnano');
const del = require('del');
const runSequence = require('run-sequence');
const sourcemaps = require('gulp-sourcemaps');
const autoprefixer = require('gulp-autoprefixer');

gulp.task('sass', function() {
  return gulp.src(`src/*.scss`)
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('dist'))
})

gulp.task('optimize', function () {
    return gulp.src('dist/*.css')
        .pipe(sourcemaps.init())
        .pipe(autoprefixer())
        .pipe(cssnano())
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('dist'));
});

gulp.task('clean', function() {
  return del.sync('dist')
})

gulp.task('default', function(callback) {
  runSequence(
    'clean',
    'sass',
    'optimize',
    callback
  )
})
