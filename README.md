![](logo.png)

This is a concatenation of styles which I use all the time:
* [Normalize.css](github.com/necolas/normalize.css) – v8.0.0
* Boostrap Grid Only
* My Offsets
* My Default Layout
* My Breakpoints Variables

# How to use

1. Install package to your project:
```
npm install zmnv-style-defaults
```

2. Import **ZMNV Style Defaults** to your project:

```css
/* If you want to import minified .css file: */
@import 'node_modules/zmnv-style-defaults/dist/defaults.css';
/* or: */
@import url("node_modules/zmnv-style-defaults/dist/defaults.css");

/* If you need to import .scss source: */
@import 'node_modules/zmnv-style-defaults/src/defaults.scss';
```

3. \* You can import only part of styles:

```css
/* defaults.css */

@import 'normalize.scss';
@import 'bootstrap-grid.scss';
@import 'offsets.scss';
@import 'layout.scss';
```

# Offsets

Classname pattern:
```{type}-{orientation}-{value}```

### {type}
* Margins: `mar-`
* Paddings: `pad-`

### {orientation}
* all: clear
* left: `-l-`
* top: `-t-`
* right: `-r-`
* bottom: `-b-`

### {value} px: 0, 2, 4, 8, 16, 24, 32, 48, 64

## Examples

```css
.mar-0 { margin: 0px !important }
.mar-t-2 { margin-top: 2px !important }
.pad-r-16 { padding-right: 16px !important }
```

# Default Layout

* `.container-width-max`
* `.container-width-middle`
* `.container-width-min`
* `.flex-left-center`
